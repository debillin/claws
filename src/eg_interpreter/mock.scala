package eg_interpreters

import billco.InteractModule._
import cats.data.State
import cats.~>

object MockInterpreter {

  case class Mock(in: List[String], out: List[String])

  object Mock {

    def read(mock: Mock): (Mock, String) = 
      mock.in match {
        case Nil    => (mock, "")
        case h :: t => (mock.copy(in = t), h)
      }

    def write(value: String)(mock: Mock): Mock = mock.copy(out = value :: mock.out)
  }

  type MockState[A] = State[Mock, A]

  def terminalToState: TerminalOp ~> MockState = new (TerminalOp ~> MockState) {
    def apply[A](t: TerminalOp[A]): MockState[A] = t match {
      case ReadLine     => State(Mock.read)
      case WriteLine(s) => State.modify(Mock.write(s))
    }
  }
}
