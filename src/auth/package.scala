package billco

import scala.language.higherKinds
import scala.annotation.tailrec
import billco.coproduct._
import cats.effect.IO
import cats.{InjectK, ~>}
import cats.free.Free


package object AuthModule {

type UserID = String
type Password = String
type Permission = String
case class User(id: String)

sealed trait Auth[A]
case class Login(u: UserID, p: Password) extends Auth[Option[User]]
case class HasPermission(u: User, p: Permission) extends Auth[Boolean]

type AuthPrg[A] = Free[Auth, A]

class Auths[F[_]](implicit I: InjectK[Auth,F]) {
  def login(id: UserID, pwd: Password): AuthPrg[Option[User]] = lift(Login(id, pwd))
  def hasPermission(u: User, p: Permission): AuthPrg[Boolean] = lift(HasPermission(u, p))
}

object Auths {
  implicit def instance[F[_]](implicit I: InjectK[Auth,F]): Auths[F] = new Auths[F]
}

  val TestAuth: Auth ~> IO = new (Auth ~> IO) {
    def apply[A](a: Auth[A]) = a match {
      case Login(uid, pwd) =>
        if (uid == "john.snow" && pwd == "Ghost")
          IO{
            Some(User("john.snow"))
          }
        else IO{
          None
        }
      case HasPermission(u, _) =>
        IO{
          u.id == "john.snow"
        }
    }
  }

}
