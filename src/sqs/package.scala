package billco

import java.io.File

import billco.coproduct._
import java.net.URL
import java.util.Date

import cats.free.Free
import cats.InjectK

package object sqs {

  sealed trait Sqs[A]
  case class Put(queue: String, message: String) extends Sqs[SendMessageResult]
  case class SendMessageResult(md5OfMessageAttributes: String, md5ofBody: String, messageId: String)

  case class Get(queue: String) extends Sqs[ReceiveMessageResult]
  case class ReceiveMessageResult(messages: Seq[Message])
  case class Message(attributes: Map[String, String], body: String, md5OfBody: String, messageId: String, receiptHandle: String)

  case class CreateQueue(name: String) extends Sqs[Unit]

  class AmazonSqsModule[F[_]](implicit I: InjectK[Sqs,F]) {

    def put(queue:String, message:String): Free[F, SendMessageResult] = lift(Put(queue, message))

    def get(queue: String): Free[F, ReceiveMessageResult] = lift(Get(queue))

    def createQueue(name: String): Free[F, Unit] = lift(CreateQueue(name))
  }

  type SqsIO[A] = Free[Sqs, A]

  object AmazonSqsModule {
    implicit def instance[F[_]](implicit I: InjectK[Sqs,F]): AmazonSqsModule[F] = new AmazonSqsModule[F]
  }

  //implicit val monadSqsIO: Monad[SqsIO] =
    //Free.freeMonad[({type λ[α] = Coyoneda[Sqs, α]})#λ]
}
