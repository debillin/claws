package billco

import java.net.URI

import billco.workflow.{Submit, Workflow}
import cats.effect.IO
import cats.~>
import cromwell.core.WorkflowSourceFilesCollection
import cromwell.engine.workflow.workflowstore.{WorkflowStoreActor, WorkflowStoreSubmitActor}

package object workflow {

  //API
  sealed trait Workflow[A]

  case class Submit(workflow: String, workflowInputs: Map[String, URI], workflowDependendencies: Map[String, ByteVector], metadata: String) extends Workflow[SubmitResult]

  case class Status(uuid: List[String]) extends Workflow[StatusResult]

  case class ListWorkflows() extends Workflow[ListWorkflowsResult]

  case class Metadata(uuid: String) extends Workflow[MetadataResult]


  //Model
  case class SubmitResult(uuids: List[String])

  case class StatusResult(uuids: List[String])

  case class ListWorkflowsResult(uuids: List[String])

  case class MetadataResult(json: String)
}

case class workflowtoIo(storage: Storage) extends (Workflow ~> IO) {
  val submitCodec = Encoder[Submit]
  override def apply[A](fa: Workflow[A]): IO[A] = fa match {
    case s@Submit(workflow, workflowInputs, workflowDependendencies, metadata) =>
      val binary = submitCodec.encode(s)
      def program[App[_]](implicit gcs: GCSModule[App]) = {
        import gcs._
      }
  }
}
