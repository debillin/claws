package billco

import java.io.File

import billco.coproduct._
import java.net.URL
import java.util.Date

import cats.InjectK
import cats.free.Free

package object gcs {

  sealed trait GCS[A]
  type GCSIO[A] = Free[GCS, A]

  case class Put(bucket: String, path: String, content: ByteVector) extends GCS[PutResult]

  case class PutResult()

  class GCSModule[F[_]](implicit I: InjectK[GCS,F]) {

    def put(bucket: String, path: String, content: ByteVector): GCSIO[PutResult] = lift(Put(bucket, path, content))
  }
}

// Imports the Google Cloud client library
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

case class GCS(storage: Storage) extends GCS ~> IO {
  def apply[A](in: GCS[A]):IO[A] = in match {
    case Put(bucket, path, content) =>
      val blobInfo = new BlobInfo.Builder().setBlobId(BlobId.of(bucket, path)).build
      IO{ storage.create(blobInfo, content.toArray) }
  }
}

    Storage storage = StorageOptions.getDefaultInstance().getService();
