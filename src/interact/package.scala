package billco

import scala.language.higherKinds
import scala.annotation.tailrec
import billco.coproduct._
import cats.effect.IO
import cats.{Inject, InjectK, ~>}
import cats.free.Free

package object InteractModule extends App {

  sealed trait TerminalOp[A]
  case object ReadLine extends TerminalOp[String]
  case class  WriteLine(value: String) extends TerminalOp[Unit]

  //type TerminalIO[A] = Free[TerminalOp, A]

  //implicit val monadTerminalIO: Monad[TerminalIO] =
    //Free.freeMonad[({type λ[α] = Coyoneda[TerminalOp, α]})#λ]

 class Interacts[F[_]](implicit I: InjectK[TerminalOp, F]) {
    def readLineI: Free[F, String] = lift(ReadLine)
    def writeLineI(s: String): Free[F, Unit] = lift(WriteLine(s))
  }

  object Interacts {
    implicit def instance[F[_]](implicit I: InjectK[TerminalOp,F]): Interacts[F] = new Interacts[F]
  }

  def terminalToIO: TerminalOp ~> IO = new (TerminalOp ~> IO) {
    def apply[A](t: TerminalOp[A]): IO[A] = t match {
      case ReadLine     =>
        IO(scala.io.StdIn.readLine)
      case WriteLine(s) =>
        IO(println(s))
    }
  }

}

