package billco

import billco.s3._
import com.amazonaws.services.s3._
import com.amazonaws.auth.BasicAWSCredentials
import billco.coproduct._
import cats.effect.IO
import cats.~>

import scala.collection.JavaConverters._

case class S3Mutator(api: String, key: String) extends (S3 ~> IO) {

  lazy val s3Client = new AmazonS3Client(new BasicAWSCredentials(api, key))

  def apply[A](x: S3[A]) = x match {
    case Get(bucket, path, matchingETagConstraints, modifiedSinceConstraint, nonMatchingETagConstraints, range, responseHeaders, s3ObjectId, sseCustomerKey, unmodifiableSince, versionId) => 
    IO {
      val mr = new com.amazonaws.services.s3.model.GetObjectRequest(bucket, path)
      matchingETagConstraints map(_.asJava) foreach mr.setMatchingETagConstraints
      modifiedSinceConstraint foreach mr.setModifiedSinceConstraint
      nonMatchingETagConstraints map(_.asJava) foreach mr.setNonmatchingETagConstraints
      range foreach{ case (low, high) => mr.setRange(low, high)}

      def sho[A](get: (ResponseHeaderOverrides) => Option[A], set: A => Unit) = 
        responseHeaders.flatMap(get) foreach set

      val rh = new com.amazonaws.services.s3.model.ResponseHeaderOverrides()
      sho(_.cacheControl, rh.setCacheControl)
      sho(_.contentDisposition, rh.setContentDisposition)
      sho(_.contentEncoding, rh.setContentEncoding)
      sho(_.contentLanguage, rh.setContentLanguage)
      sho(_.contentType, rh.setContentLanguage)

      val gor = s3Client.getObject(mr)
      import gor._

      val mt= gor.getObjectMetadata()
      import mt._

      val md = Metadata(cacheControl = getCacheControl, contentDisposition = getContentDisposition, contentEncoding = getContentEncoding,
        contentLength = getContentLength, contentMd5 = getContentMD5, contentType = getContentType, eTag = getETag, expirationTime = getExpirationTime,
        expirationTimeRuleId = getExpirationTimeRuleId, httpExpiresDate = getHttpExpiresDate, instanceLength=getInstanceLength, lastModified=getLastModified,
        ongoingRestore=getOngoingRestore, rawMetadata=scala.collection.immutable.Map(getRawMetadata.asScala.toSeq:_*), restoreExpirationTime = getRestoreExpirationTime,
        sseAlgorithm = getSSEAlgorithm, sseCustomerAlgorithm = getSSECustomerAlgorithm, sseCustomerKeyMd5 = getSSECustomerKeyMd5, versionId = getVersionId)

      val content = getObjectContent
      val stream = S3Stream(content.abort, content.getHttpRequest.toString, content)
      billco.s3.GetObjectResult(bucket = bucket, key = path, content = stream, metadata = md)
    }

    case Put(bucket, path, file) => IO {
      val pmr = new com.amazonaws.services.s3.model.PutObjectRequest(bucket, path, file)
      val por = s3Client.putObject(pmr)
      import por._
      billco.s3.PutObjectResult(contentMd5 = getContentMd5, eTag=getETag, expirationTime = getExpirationTime, expirationTimeRuleId = getExpirationTimeRuleId,
        versionId = getVersionId)
    }
  }

  def presignedUrl(bucket: String, path: String) = {
    val pur = new com.amazonaws.services.s3.model.GeneratePresignedUrlRequest(bucket, path)
    val gqurr = s3Client.generatePresignedUrl(pur)
    gqurr
  }
}
