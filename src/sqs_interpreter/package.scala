package billco.sqs_interpreters

import billco.coproduct._
import billco.sqs._
import cats.effect.IO
import cats.~>
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.sqs._
import com.amazonaws.services.sqs.model.{GetQueueUrlRequest, ReceiveMessageRequest, SendMessageRequest}

import scala.collection.JavaConverters._

case class SqsRunner(api: String, key: String) extends (Sqs ~> IO) {

  lazy val sqsClient = new AmazonSQSClient(new BasicAWSCredentials(api, key))

  def apply[A](x: Sqs[A]) = x match {
    case Get(queueName) =>  
      val url = queueUrl(queueName)
      val mr = new ReceiveMessageRequest(url)
      IO {val rmr = sqsClient.receiveMessage(mr)
      ReceiveMessageResult(
        rmr.getMessages.asScala.map(x => Message(
          attributes = collection.immutable.Map(x.getAttributes.asScala.toSeq:_*),
          body = x.getBody,
          md5OfBody = x.getMD5OfBody,
          messageId = x.getMessageId,
          receiptHandle = x.getReceiptHandle)).toSeq)
      }
    case Put(queueName, message) =>
      val url = queueUrl(queueName)
      val pmr = new SendMessageRequest(url, message)
     IO { 
      val smr = sqsClient.sendMessage(pmr)
      SendMessageResult(smr.getMD5OfMessageAttributes, smr.getMD5OfMessageBody, smr.getMessageId)
      }
    //TODO
    case CreateQueue(name) => IO(())
  }

  def queueUrl(queueName: String) = {
      val gqur = new GetQueueUrlRequest(queueName)
      val gqurr = sqsClient.getQueueUrl(gqur)
      gqurr.getQueueUrl
    }
}
