package billco

import scala.language.higherKinds
import scala.annotation.tailrec
import billco.coproduct._
import java.io.BufferedReader
import java.io.FileReader
import java.io.FileWriter

import cats.effect.IO
import cats.{InjectK, ~>}
import cats.free.Free
import java.net.URI
import cats.free.Cofree
import fs2.Stream

package object filez extends App {

  sealed trait FilezOp[A]

  case class WriteFile(value: ByteVector, fileName: String, openOptions: Seq[java.nio.file.OpenOption]) extends FilezOp[Unit]

  case class WriteLine(value: String, fileName: String, openOptions: Seq[java.nio.file.OpenOption]) extends FilezOp[Unit]

  case class ReadLine(fileName: String, lineOffset: Int) extends FilezOp[String]

  case class Copy(source: URI, destination: URI, overwrite: Boolean) extends FilezOp[Unit]

  case class FullContent(source: URI) extends FilezOp[String]

  //TODO: Refine type to be Positive
  case class Size(source: URI) extends FilezOp[Long]

  case class Delete(file: URI) extends FilezOp[Unit]

  case class Hash(file: URI) extends FilezOp[String]

  case class Touch(file: URI) extends FilezOp[Unit]

  case class Exists(file: URI) extends FilezOp[Boolean]

  //Returns string multiple times, e.g. in a fs2.Stream[IO, String]
  case class ReadLines(file: URI) extends FilezOp[String]


  type FilezIO[A] = Free[FilezOp, A]

  case class FilezModule[F[_]](implicit i: InjectK[FilezOp, F]) {
    def write(value: String, fileName: String, openOptions: Seq[java.nio.file.OpenOption] = Seq.empty): Free[F, Unit] = lift(WriteLine(value, fileName, openOptions))
    def read(fileName: String, byteOffset: Int = 0): Free[F,String] = lift(ReadLine(fileName, byteOffset))
  }

  //implicit val monadTerminalIO: Monad[FilezIO] =
    //Free.freeMonad[({type λ[α] = Coyoneda[FilezOp, α]})#λ]

  object FilezModule {
    implicit def instance[F[_]](implicit I: InjectK[FilezOp,F]): FilezModule[F] = new FilezModule[F]
  }


  //   Interpreters

  def filezOpToIO: FilezOp ~> IO = new (FilezOp ~> IO) {
    def apply[A](t: FilezOp[A]): IO[A] = t match {
      case ReadLine(file, lineOffset) =>
        IO{
          val f = new BufferedReader(new FileReader(file))
          for (i <- 0 to lineOffset - 1) f.readLine
          val _return = f.readLine()
          f.close()
          _return
        }

      case WriteLine(input, filename) => {
        IO{
          val f = new FileWriter(filename)
          f.write(input)
          f.close()
        }
      }
    }
  }
}
