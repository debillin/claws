package billco

import cats.{Id, InjectK, ~>}
import cats.free.Free
import cats.effect.IO

package object coproduct {

  def lift[F[_],G[_],A](f: F[A])(implicit I: InjectK[F,G]): Free[G,A] =
    Free.liftF(I.inj(f))

  val ioTrans = new (IO ~> Id) {
    def apply[A](fa: IO[A]): Id[A] = fa.unsafeRunSync()
  }
}
