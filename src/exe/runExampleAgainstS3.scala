/*
import java.io.FileWriter

import billco.InteractModule._
import billco.s3._
import billco.coproduct._
import billco.S3Mutator

val file = new java.io.File("file.dat")

type OurApp[A] = Coproduct[S3, TerminalOp, A]

def program[F[_]](implicit s3Injection: S3Module[F], interactsInjection: Interacts[F]) = {
  import s3Injection._
  import interactsInjection._

  for {
    _       ← writeLineI("Writing to S3")
    s3Thing ← put("sandbox-dbillings", "dbillings/test4", file)
    _       ← writeLineI(s"$s3Thing")
  } yield ()
}

val interpreter: OurApp ~> IO = S3Mutator("","") or terminalToIO

val coyoint: ({type λ[x] = Coyoneda[OurApp, x]})#λ ~> IO = Coyoneda.liftTF(interpreter)

val prog: Free[IO, Unit] = program[OurApp].mapSuspension(coyoint)

prog.foldMap(ioTrans)
*/
