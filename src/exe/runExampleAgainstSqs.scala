import java.io.FileWriter

import billco.InteractModule._
import billco.sqs._
import billco.s3._
import billco.sqs_interpreters._
import billco.coproduct._
import billco.S3Mutator
import cats.syntax.traverse._
import cats.instances.list._
import cats.{Applicative, ~>}
import cats.data.EitherK
import cats.effect.IO
import cats.free.{Coyoneda, Free}

import scala.util.parsing.json.JSON

object X extends App {


val file = new java.io.File("file.dat")

type OurApp[A] = EitherK[Sqs, TerminalOp, A]

type OurAppCoyo[A] = Coyoneda[OurApp, A]

type OurFree[A] = Free[OurAppCoyo,A]

def program(implicit s3Injection: AmazonSqsModule[OurApp], interactsInjection: Interacts[OurApp]) = {
  import s3Injection._
  import interactsInjection._


  for {
    _       ← writeLineI("getting from Sqs")
    sqsMsgs ← get("sandbox_dbillings")
    _       ← writeLineI(s"raw data $sqsMsgs")
    _       ←  sqsMsgs.messages.toList.traverse {
                 msg => 
                   val s3Report = S3Report(msg.body)
                   writeLineI(s"message was $s3Report")
               }
  } yield ()
}

val interpreter: OurApp ~> IO = SqsRunner("AKIAJQOUM724NKMVDSUA","Dp+ASwt2MMmr9tG0c1DCUMImC2Ej1nPQLdoJDEfh") or terminalToIO

//val coyoint: OurAppCoyo ~> IO = Coyoneda.liftTF(interpreter)
//val coyoint: OurAppCoyo ~> IO = ??? //Coyoneda.lift(interpreter)

//val prog: Free[IO, Unit] = program.mapSuspension(coyoint)
val prog: Free[IO, Unit] = program.mapK(interpreter)

prog.foldMap(ioTrans)
}
