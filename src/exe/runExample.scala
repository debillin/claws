/*
import billco.AuthModule.{HasPermission, Auth, Auths}
import billco.AuthModule._
import billco.InteractModule._
import eg_interpreters.MockInterpreter.Mock
import eg_interpreters.MockInterpreter._
import scalaz.effect.IO
import scalaz.{~>, Coyoneda, Free, Coproduct}
import scalaz.Free.{Return, FreeC}
import scalaz.Free.return_
import billco.coproduct._

val KnowSecret = "KnowSecret"

def program[F[_]](implicit authsInjection: Auths[F], interactsInjection: Interacts[F]) = {
  import authsInjection._
  import interactsInjection._

  //Now we're cooking with gas

  for {
    _     ← writeLineI("Enter User")
    uid   ← readLineI
    _     ← writeLineI("Enter Pass")
    paswd ← readLineI
    u     ← login(uid, paswd)
    _     ← writeLineI(s"$u")
  } yield ()
}

val init = Mock(
  in = List("Hello", "World"),
  out = List()
)

type  OurApp[A] = Coproduct[Auth, TerminalOp, A]

val interpreters: OurApp ~> IO = TestAuth or terminalToIO

val coyoint: ({type f[x] = Coyoneda[OurApp, x]})#f ~> IO = Coyoneda.liftTF(interpreters)

val my = program[OurApp]

val prog: Free[IO, Unit] = my.mapSuspension(coyoint)

prog foldMap ioTrans
*/
