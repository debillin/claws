/*
import cats.data.EitherK
import cats.data.Inject._
import billco.InteractModule._
import cats.effect.IO
import billco.InteractModule
import billco.sqs._
import billco.s3._
import billco.s3.S3Module
import billco.sqs_interpreters._
import cats.free.{~>, Coyoneda, Free}
import billco.coproduct._
import billco.S3Mutator

val file = new java.io.File("file.dat")

val key = "AKIAJAAUMDBPXYZUP7PA"
val secret = "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"

type C1[A]         = EitherK[TerminalOp, S3, A]
type OurApp[A]     = EitherK[Sqs, C1, A]

type OurAppCoyo[A] = Coyoneda[OurApp, A]

type OurFree[A]    = Free[OurAppCoyo,A]

def program[App[_]](implicit sqsInjection: AmazonSqsModule[OurApp], s3Injection: S3Module[OurApp], interactsInjection: Interacts[OurApp]) = {
  import sqsInjection._
  import interactsInjection._
  import s3Injection._

  val ap = Applicative[OurFree]

  for {
    _       ← writeLineI("enter filename")
    data    ← readLineI
    _       ← s3Injection.put("sandbox-dbillings", "dbillings/tests5", file)
    _       ← writeLineI("getting from Sqs")
    sqsMsgs ← sqsInjection.get("sandbox_dbillings")
    _       ← writeLineI(s"raw data $sqsMsgs")
    _       ← ap.traverse(sqsMsgs.messages.toList) {
                msg =>
                  val s3Report = S3Report(msg.body)
                  writeLineI(s"message was $s3Report")
              }
  } yield ()
}

val interpreter1: C1 ~> IO = terminalToIO or S3Mutator(key, secret)
val interpreter:  OurApp ~> IO = SqsRunner(key, secret) or interpreter1
val interpreter2:  IO ~> Task = new (IO ~> Task) {
  def apply[A](x: IO[A]): Task[A] = Task.delay{x.unsafePerformIO}
}

val coyoint: OurAppCoyo ~> IO = Coyoneda.liftTF(interpreter)

val prog: Free[IO, Unit] = program(AmazonSqsModule.instance, S3Module.instance[OurApp], Interacts.instance).mapSuspension(coyoint)

println(prog.foldMap(interpreter2))

println("all done")
*/
