/*
import scala.language.higherKinds
import billco.filez._
import billco.InteractModule._
import eg_interpreters.MockInterpreter.Mock
import eg_interpreters.MockInterpreter._
import scalaz.concurrent.Task
import scalaz.effect.IO
import scalaz.{~>, Coyoneda, Free, Coproduct}
import scalaz.Free.{Return, FreeC}
import scalaz.Free.return_
import scalaz.\/._
import billco.coproduct._

def program[F[_]](implicit filezInjection: FilezModule[F], interactsInjection: Interacts[F])  = {
  import filezInjection._
  import interactsInjection._

  for {
    uid ← read("some.dat", 0)
    _   ← writeLineI(s"line was $uid")
  } yield s"I Said the line was $uid"
}

def writeStuffToFile[F[_]](implicit filezInjection: FilezModule[F], interactsInjection: Interacts[F])  = {
  import filezInjection._
  import interactsInjection._

  for {
    _ ← writeLineI("enter some data")
    s ← readLineI
    _ ← writeLineI("enter filename")
    f ← readLineI
    _ ← write(s, f)
  } yield ()
}

def r[F[_]](implicit filezInjection: FilezModule[F], interactsInjection: Interacts[F])  = {
  import filezInjection._
  import interactsInjection._

  for {
    _ ← writeLineI("enter some data")
    s ← readLineI
    _ ← writeLineI("enter filename")
    f ← readLineI
    _ ← write(s, f)
  } yield ()
}

type OurApp[A] = Coproduct[FilezOp, TerminalOp, A]

def my = program[OurApp]

def my2 = writeStuffToFile[OurApp]

val interpreters: OurApp ~> IO = filezOpToIO or terminalToIO

val coyoint: ({type f[x] = Coyoneda[OurApp, x]})#f ~> IO = Coyoneda.liftTF(interpreters)

val prog: Free[IO, String] = my.mapSuspension(coyoint)

val prog2: Free[IO, Unit] = my2.mapSuspension(coyoint)

my.foldMap(coyoint).unsafePerformIO
//my2.foldMap(coyoint).unsafePerformIO

//prog2.foldMap(ioTrans)

*/
