package billco

package object jobstore {

  sealed trait JobStore[A]

  case class QueryCommandCompletion(workflowId: String, callFqn: String, index: Option[Int]) extends JobStore[QueryCommandCompletionResponse]

  sealed trait QueryCommandCompletionResponse
  case class JobComplete(result: JobResult)

  sealed trait JobResult
  case class JobSuccess() extends JobResult
  case class JobFail() extends JobResult

}
