package billco

import java.io.File

import billco.coproduct._
import java.net.URL
import java.util.Date

import cats.InjectK
import cats.free.Free

package object s3 {
  
  sealed trait S3[A]

  case class Put(bucket: String, path: String, file: File) extends S3[PutObjectResult]

  /**
    * @param range inclusive byte range within the desired object that will be downloaded by this request [[http://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/services/s3/model/GetObjectRequest.html#getBucketName() original]]
    */
  case class Get(bucket: String, path: String, matchingETagConstraints: Option[Seq[String]],
                 modifiedSinceConstraint:Option[Date], nonMatchingETagConstraints:Option[Seq[String]], range: Option[(Long, Long)], 
                 responseHeaders: Option[ResponseHeaderOverrides], s3ObjectId: Option[S3ObjectId], sseCustomerKey: Option[SSECustomerKey],
                 unmodifiableSince: Option[Date], versionId: Option[String]
  ) extends S3[GetObjectResult]

  case class ListObjects(bucket: String, path: String, delimiter: Option[String], marker: Option[String], prefix :Option[String]) extends S3[ListObjectsResult]

  class S3Module[F[_]](implicit I: InjectK[S3,F]) {

    def put(bucket: String, path: String, file: File): S3IO[PutObjectResult] = lift(Put(bucket, path, file))

    def get(bucket: String, path: String, etags: Option[Seq[String]] = None, modifiedSinceConstraint: Option[Date] = None): S3IO[GetObjectResult] =
      lift(Get(bucket, path, etags, modifiedSinceConstraint, None, None, None, None, None, None, None))

    def list(bucket: String, path: String, delimiter: Option[String] = None, marker: Option[String] = None, prefix: Option[String] = None):
              S3IO[ListObjectsResult] =
      lift(ListObjects(bucket, path, delimiter, marker, prefix))
  }

  object S3SchemaVersion extends Enumeration {
    type S3SchemaVersion = Value 
    val v1 = Value
  }
  import S3SchemaVersion._

  def parseSchema(in: String): S3SchemaVersion = 
    in match {
      case "1.0" => S3SchemaVersion.v1
      case _ => S3SchemaVersion.v1
    }


  object ConfigurationId extends Enumeration {
    type ConfigurationId = Value 
    val SendWhenFinished = Value
  }
  import ConfigurationId._

  object AwsRegion extends Enumeration {
    type AwsRegion = Value 
    val UsEast1 = Value
  }
  import AwsRegion._

  def parseAwsRegion(in: String): AwsRegion = 
    in match {
      case "us-east-1" => AwsRegion.UsEast1
      case _ => AwsRegion.UsEast1
    }

  case class S3Report(eventName: String, sourceIpAddress: Option[String], requestParameters: Map[String, Any], s3SchemaVersion: S3SchemaVersion,
    configurationId: ConfigurationId, bucketName: String, bucketOwner: String, arn: String, key : String, size: Double, eTag: String, userIdentity: String,
    eventVersion: String, xAmzRequestId: String, xAmzId2: String, awsRegion: AwsRegion, eventTime: String)
                    
  object S3Report {
    def apply(input: String): Seq[S3Report] = {
      val s3Report = scala.util.parsing.json.JSON.parseFull(input)
      s3Report match {
        case Some(map) => 

          //ouch
          val records = map.asInstanceOf[Map[String, Any]].get("Records").asInstanceOf[Option[List[Map[String, Any]]]].get

          records.map{ record =>
            val s3 = record("s3").asInstanceOf[Map[String, Any]]
            val responseElements = record("responseElements").asInstanceOf[Map[String, Any]]
            val bucket = s3("bucket").asInstanceOf[Map[String, Any]]
            val s3Object = s3("object").asInstanceOf[Map[String, Any]]
            val rq = record("requestParameters").asInstanceOf[Map[String, Any]]

            S3Report(
              eventName = record("eventName").toString,
              sourceIpAddress = rq.get("sourceIPAddress").map(_.toString),
              requestParameters = rq,
              s3SchemaVersion = parseSchema(s3("s3SchemaVersion").toString),
              configurationId = ConfigurationId.withName(s3("configurationId").toString),
              bucketName = bucket("name").toString,
              bucketOwner = bucket("ownerIdentity").toString,
              arn = bucket("arn").toString,
              key = s3Object("key").toString,
              size = s3Object("size").toString.toDouble,
              eTag = s3Object("eTag").toString,
              userIdentity = record("userIdentity").toString,
              eventVersion = record("eventVersion").toString,
              xAmzRequestId = responseElements("x-amz-request-id").toString,
              xAmzId2 = responseElements("x-amz-id-2").toString,
              awsRegion = parseAwsRegion(record("awsRegion").toString),
              eventTime = record("eventTime").toString
            )
          }
        case None => Seq()
      }
    }
  }

  type S3IO[A] = Free[S3, A]

  object Algorithm extends Enumeration {
    type Algorithm = Value 
    val Aes256 = Value
  }
  import Algorithm._

  case class SSECustomerKey(algo: Algorithm)

  case class S3ObjectId(bucket: String, key: String, versionId: String, instructionFileId: InstructionFileId)

  case class PutObjectResult(contentMd5: String, eTag: String, expirationTime: java.util.Date, expirationTimeRuleId: String,
    versionId: String)

  case class GetObjectResult(bucket: String, key: String, content: S3Stream, metadata: Metadata)

  case class ListObjectsResult(bucket: String, commonPrefixes: Seq[String], delimiter: String, encodingType: String, marker: String,
    maxKeys: Int, nextMarker: String, objectSummaries: Seq[S3ObjectSummary], prefix: String)

  case class ResponseHeaderOverrides(cacheControl: Option[String] = None, contentDisposition: Option[String] = None, contentEncoding: Option[String] = None,
    contentLanguage: Option[String] = None, contentType: Option[String] = None, expires: Option[String] = None)

  case class InstructionFileId()

  object StorageClass extends Enumeration {
    type StorageClass = Value
    val S3, Glacier = Value
  }

  import StorageClass._

  case class S3ObjectSummary(bucketName: String, etag: String, key: String, lastModified: Date, owner: Owner, size: Long, storageClass: StorageClass)

  case class Owner(displayName: String, id: String)

  case class S3Stream(abort: () => Unit, httpRequest: String, inputStream: java.io.InputStream)

  case class Metadata(cacheControl: String, contentDisposition: String, contentEncoding: String, contentLength:Long, contentMd5: String, contentType: String,
    eTag: String, expirationTime:Date, expirationTimeRuleId: String, httpExpiresDate: Date, instanceLength: Long, lastModified: Date, ongoingRestore: Boolean,
    rawMetadata: Map[String, java.lang.Object], restoreExpirationTime: Date, sseAlgorithm: String, sseCustomerAlgorithm: String, sseCustomerKeyMd5: String,
    versionId: String)

  object S3Module {
    implicit def instance[F[_]](implicit I: InjectK[S3,F]): S3Module[F] = new S3Module[F]
  }

  //implicit val monadS3IO: Monad[S3IO] =
    //Free.freeMonad[({type λ[α] = Coyoneda[S3, α]})#λ]

}

