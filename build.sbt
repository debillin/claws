resolvers += "broad" at "https://broadinstitute.jfrog.io/broadinstitute/libs-release/"

scalaVersion := "2.12.6"

scalaSource in Compile := {baseDirectory.value / "src"}

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-free" % "1.1.0",
  "com.google.cloud" % "google-cloud" % "0.47.0-alpha")

libraryDependencies += "org.typelevel" %% "cats-effect" % "1.0.0-RC"

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.9.13"

libraryDependencies += "org.scodec" %% "scodec-bits" % "1.1.5"

libraryDependencies += "org.broadinstitute" %% "cromwell" % "31.1"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

libraryDependencies += "co.fs2" %% "fs2-core" % "0.10.4"

scalacOptions ++= Seq(
  "-Ypartial-unification"             // Enable partial unification in type constructor inference
)
